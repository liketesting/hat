__version__ = "2.0.1"
__author__ = "ArtLinty"

# MIT License
#
# Copyright (c) 2019 立师兄Linty
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""
case 包 构造方法，包含
    WebCase：WebUI 用例继承的基类
    ApiCase：WebAPI 用例继承的基类
    
！！！ 请勿直接使用 _BaseCase
"""

import time

import allure
import pytest

from base import build_logger, build_driver, parse_json, BoxDriver, BoxRequest, build_request
from base.helper import parse_digit, PathHelper, JsonHelper, YamlHelper


class __BaseCase(object):
    """
    私有类，仅供本类继承，请勿直接使用使用
    """
    _logger = None

    def init_logger(self, current_file_name):
        """
        初始化用例执行的日志 logger，子类需要调用的方法
        :param current_file_name: 当前 用例的文件名，请直接传递  __name__
        :return: 无
        """
        if current_file_name and isinstance(current_file_name, str):
            if ".log" in current_file_name:
                current_file_name = current_file_name.replace(".log", "")

            current_file_name = "report/log/%s_%s.log" % (current_file_name, self.current_time_string)

        self._logger = build_logger(
            PathHelper.get_actual_path_by_project_root(current_file_name)
        )

    @property
    def current_time_string(self):
        """
        当前时间的字符串格式
        :return:
        """
        return time.strftime("%Y%m%d%H%M%S", time.localtime())

    @property
    def current_timestamp(self):
        """
        当前时间戳
        :return: 精确到整数
        """
        return round(time.time(), 0)

    @property
    def logger(self):
        """
        当前用例的 日志 logger
        :return:
        """
        return self._logger

    def info(self, msg):
        """
        添加日志
        :param msg:
        :return:
        """
        if self._logger is not None:
            self._logger.info(msg)

    def assert_in(self, expected, actual):
        """
        assert in, expected in actual
        :param expected:
        :param actual:
        :return:
            例如
                "胡艳" in "胡晓艳"  fail
                "胡" in "胡晓艳"  success
        """
        result = expected in actual
        log_msg = "断言 assert_in：{%r} in {%r} 的结果是 {%r}！" % (expected, actual, result)

        self.info(log_msg)
        return result

    def assert_equal(self, expected, actual):
        """
        assert equal
        :param expected:
        :param actual:
        :return: 类型要一致
            例如
                "1" == 1  fail
                1 == 1 success
                str, int, float, boolean( True, False)...
                object 类型不可以相等
                    api1 = NspUserApi()
                    api2 = NspUserApi()
                    api1 == api2  fail
        """
        result = expected == actual
        log_msg = "断言 assert_equal：{%r} == {%r} 的结果是 {%r}！" % (expected, actual, result)

        self.info(log_msg)
        return result

    def assert_int_equal(self, expected, actual):
        """
        assert int equal, 忽略字符串和数字之间的差异
        :param expected:
        :param actual:
        :return:
            例如
                assert_int_equal(1, "1") success
                assert_int_equal(1.0, "1")  success
                assert_int_equal(-1.0, "-1")  success
                assert_int_equal(-1.0, "-1.0")  success
                assert_int_equal(-1, "-1.0")  success
        """
        result = False

        expected = self.str_to_decimal(expected, ndigits=0)
        actual = self.str_to_decimal(actual, ndigits=0)

        if expected is not None and actual is not None:
            result = expected == actual

        log_msg = "断言 assert_int_equal：{%r} == {%r} 的结果是 {%r}！" % (expected, actual, result)

        self.info(log_msg)
        return result

    def assert_decimal_equal(self, expected, actual, ndigits=2):
        """
        assert int equal, 忽略字符串和数值的差别
        :param ndigits: 小数位数
        :param expected:
        :param actual:
        :return:
            例子
                assert_decimal_equal("123.88", 123.88, 2) success
                assert_decimal_equal("123.88", 123.88, 1) success
                assert_decimal_equal("123.88", 123.89, 1) success
                assert_decimal_equal("123.88", 123.89, 2) fail
        """
        result = False

        expected = self.str_to_decimal(expected, ndigits)
        actual = self.str_to_decimal(actual, ndigits)

        if expected is not None and actual is not None:
            result = expected == actual

        log_msg = "断言 assert_decimal_equal：小数位{%r}, {%r} == {%r} 的结果是 {%r}！" \
                  % (ndigits, expected, actual, result)

        self.info(log_msg)
        return result

    def assert_json_equal(self, expected, actual, data_key, index=None, sub_key=None):
        """
        断言 字典是否相等
        :param expected: 期望的结果 dict 格式
        :param actual: 实际的结果 dict 格式
        :param data_key: data key
        :param index: 下标，默认为 None
        :param sub_key: 子 data key，默认为 None
        :return:
        """
        result = False
        expected_value = parse_json(expected, data_key, index, sub_key)
        actual_value = parse_json(actual, data_key, index, sub_key)

        if expected_value is not None and actual_value is not None:
            result = expected_value == actual_value

        log_msg = "断言 assert_dict_equal： {%r} == {%r}, data_key=%s, index=%r, sub_key=%s 的结果是 {%r}！" \
                  % (expected, actual, data_key, index, sub_key, result)

        self.info(log_msg)
        return result

    def str_to_decimal(self, digit_str, ndigits=None):
        """
        转变 字符串（数字组成的字符串）为数字
        :param ndigits: 数值的小数位数，默认是 None
        :param digit_str: 数字组成的字符串
            规则：
                接受整型 "123"
                接受负数 "-123"
                接受小数 "-123.88"， "123.88"

        :return:
            如果 digit_str 是字符串 str 格式，就转化
            否则，返回 原样的值
        """
        self.info("[system] - [%s] str_to_decimal: %s" % (__name__, digit_str))
        ret_decimal = digit_str
        if digit_str is not None and isinstance(digit_str, str):
            ret_decimal = parse_digit(digit_str)
        if ndigits is not None and isinstance(ret_decimal, float) and isinstance(ndigits, int):
            ret_decimal = round(ret_decimal, ndigits)
        return ret_decimal

    def json_to_dict(self, json_str_to_convert):
        """
        转 json 格式的 字符串 为 Python 字典 dict 类型
        建议使用 bejson.com 编写 json，然后 压缩成为字符串，复制到变量
        :param json_str_to_convert:
        :return: dict
        """
        self.info("[system] - [%s] json_to_dict: %s" % (__name__, json_str_to_convert))
        if json_str_to_convert is not None and isinstance(json_str_to_convert, str):
            return JsonHelper.convert_json_str_to_dict(json_str_to_convert)

        return None

    def dict_to_yaml(self, dict_to_comvert):
        """
        字典转 yaml 格式
        :param dict_to_comvert:
        :return:
        """
        self.info("[system] - [%s] dict_to_yaml: %s" % (__name__, dict_to_comvert))
        if dict_to_comvert is not None and isinstance(dict_to_comvert, dict):
            return YamlHelper.dict_to_yaml(dict_to_comvert)

        return None


class WebCase(__BaseCase):
    """
    测试用例类

    """
    _driver = None

    def init_browser(self, url, browser_type="c"):
        """
        测试用例中，初始化浏览器，子类调用
        :param url: 初始化浏览器需要打开的 网址，需要包含协议（http or https）
        :param browser_type:
        :return:
        """

        if not browser_type:
            browser_type = "c"

        self._driver = build_driver(
            url=url,
            driver_type=browser_type,
            wait_seconds=5
        )

    def quit_browser(self):
        """
        退出当前用例的浏览器
        :return:
        """
        if self._driver and isinstance(self._driver, BoxDriver):
            self._driver.quit()

    @property
    def driver(self):
        """
        当前用例的 浏览器 browser
        :return:
        """
        return self._driver

    @pytest.fixture(autouse=True)
    def prepare(self):
        """
        测试装置， yield 上面是 测试前置条件，yield 下面是测试清理操作
        :return:
        """
        self.info("[%r]-start test case exec!" % __name__)
        yield
        self.info("[%r]finish test case exec!" % __name__)

    @allure.step
    def snapshot(self, name):
        """
        snapshot 对浏览器进行截图
        :return:
        """
        if self._driver is not None:
            image = self._driver.save_window_snapshot_by_png()
            allure.attach(image, name=name, attachment_type=allure.attachment_type.PNG)

    def assert_in(self, expected, actual):
        """
        assert in
        :param expected:
        :param actual:
        :return:
        """

        result = super(WebCase, self).assert_in(expected, actual)

        if not result:
            self.snapshot(self.current_time_string)

        return result

    def assert_equal(self, expected, actual):
        """
        assert equal
        :param expected:
        :param actual:
        :return:
        """
        result = super(WebCase, self).assert_equal(expected, actual)

        if not result:
            self.snapshot(self.current_time_string)

        return result

    def assert_int_equal(self, expected, actual):
        """
        assert int equal, 忽略字符串和数字之间的差异
        :param expected:
        :param actual:
        :return:
            例如
                assert_int_equal(1, "1") success
                assert_int_equal(1.0, "1")  success
                assert_int_equal(-1.0, "-1")  success
                assert_int_equal(-1.0, "-1.0")  success
                assert_int_equal(-1, "-1.0")  success
        """
        result = super(WebCase, self).assert_int_equal(expected, actual)

        if not result:
            self.snapshot(self.current_time_string)
        return result

    def assert_decimal_equal(self, expected, actual, ndigits=2):
        """
        assert int equal, 忽略字符串和数值的差别
        :param ndigits: 小数位数
        :param expected:
        :param actual:
        :return:
            例子
                assert_decimal_equal("123.88", 123.88, 2) success
                assert_decimal_equal("123.88", 123.88, 1) success
                assert_decimal_equal("123.88", 123.89, 1) success
                assert_decimal_equal("123.88", 123.89, 2) fail
        """
        result = super(WebCase, self).assert_decimal_equal(expected, actual, ndigits)

        if not result:
            self.snapshot(self.current_time_string)
        return result


class ApiCase(__BaseCase):
    """
    API case
    """

    _request = None

    def init_request(self, host, port=None):
        """
        初始化用例
        :param port:
        :param host: API 网址的主机部分，可以是 IP，也可以域名，需要包含协议（http or https）
        :return:
        """
        self._request = build_request(
            host=host,
            port=port
        )

    @property
    def request(self):
        """
        request
        :return:
        """
        return self._request

    @pytest.fixture(autouse=True)
    def prepare(self):
        self.info("[%r]-start test case exec!" % __name__)
        yield
        self.info("[%r]-finish test case exec!" % __name__)

    @allure.step
    def snapshot_request(self, name):
        """
        snapshot
        :return:
        """
        if self.request and isinstance(self.request, BoxRequest):
            json = self.request.json_string
            allure.attach(json, name="json_dict_%s" % name, attachment_type=allure.attachment_type.JSON)

            request = self.dict_to_yaml(self.request.full_request)
            allure.attach(request, name="full_request_%s" % name, attachment_type=allure.attachment_type.YAML)

            response = self.dict_to_yaml(self.request.full_response)
            allure.attach(response, name="full_response_%s" % name, attachment_type=allure.attachment_type.YAML)

    def assert_in(self, expected, actual):
        """
        assert in, expected in actual
        :param expected:
        :param actual:
        :return:
            例如
                "胡艳" in "胡晓艳"  fail
                "胡" in "胡晓艳"  success
        """
        result = super(ApiCase, self).assert_in(expected=expected, actual=actual)

        if not result:
            self.snapshot_request(self.current_time_string)
        return result

    def assert_equal(self, expected, actual):
        """
        assert equal
        :param expected:
        :param actual:
        :return: 类型要一致
            例如
                "1" == 1  fail
                1 == 1 success
                str, int, float, boolean( True, False)...
                object 类型不可以相等
                    api1 = NspUserApi()
                    api2 = NspUserApi()
                    api1 == api2  fail
        """
        result = super(ApiCase, self).assert_equal(expected=expected, actual=actual)
        if not result:
            self.snapshot_request(self.current_time_string)
        return result

    def assert_int_equal(self, expected, actual):
        """
        assert int equal, 忽略字符串和数字之间的差异
        :param expected:
        :param actual:
        :return:
            例如
                assert_int_equal(1, "1") success
                assert_int_equal(1.0, "1")  success
                assert_int_equal(-1.0, "-1")  success
                assert_int_equal(-1.0, "-1.0")  success
                assert_int_equal(-1, "-1.0")  success
        """
        result = super(ApiCase, self).assert_int_equal(expected=expected, actual=actual)

        if not result:
            self.snapshot_request(self.current_time_string)
        return result

    def assert_decimal_equal(self, expected, actual, ndigits=2):
        """
        assert int equal, 忽略字符串和数值的差别
        :param ndigits: 小数位数
        :param expected:
        :param actual:
        :return:
            例子
                assert_decimal_equal("123.88", 123.88, 2) success
                assert_decimal_equal("123.88", 123.88, 1) success
                assert_decimal_equal("123.88", 123.89, 1) success
                assert_decimal_equal("123.88", 123.89, 2) fail
        """
        result = super(ApiCase, self).assert_decimal_equal(expected=expected, actual=actual, ndigits=ndigits)

        if not result:
            self.snapshot_request(self.current_time_string)
        return result

    def assert_json_equal(self, expected, actual, data_key, index=None, sub_key=None):
        """
        断言 字典是否相等
        :param expected: 期望的结果 dict 格式
        :param actual: 实际的结果 dict 格式
        :param data_key: data key
        :param index: 下标，默认为 None
        :param sub_key: 子 data key，默认为 None
        :return:
        """
        result = super(ApiCase, self).assert_json_equal(expected=expected, actual=actual, data_key=data_key,
                                                        index=index,
                                                        sub_key=sub_key)
        if not result:
            self.snapshot_request(self.current_time_string)
        return result
