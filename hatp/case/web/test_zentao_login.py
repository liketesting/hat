import pytest
from scenario.zentao_biz import get_real_name_with_login

from base import read_csv
from case import WebCase


class TestZentaoLogin(WebCase):
    """
    author: ArtLinty
    email: hello@linty.art
    desc: 测试测试禅道登录业务
    """
    _csv_data = read_csv(
        __file__,
        "data/test_zentao_login.csv"
    )

    @pytest.fixture(autouse=True)
    def prepare(self):
        """
        测试固件
        :return:
        """

        self.init_logger(__name__)
        self.init_browser(url="http://biz.demo.zentao.net/", browser_type="c")

        yield
        self.quit_browser()

    @pytest.mark.parametrize("dt", _csv_data)
    def test_login(self, dt):
        """
        测试登录，合法数据正常登录
        :param dt:
        :return:
        """

        account = dt["account"]
        password = dt["password"]

        actual_real_name = get_real_name_with_login(
            driver=self._driver,
            account=account,
            password=password,
            logger=self._logger
        )

        self.info(
            "[%r]-登录禅道, 使用数据 account=%r, password=%r"
            % (__file__, account, password)
        )
        self.snapshot("登录后截图数据=%r" % dt)
        expected = dt["expected_url"]
        actual = self._driver.get_url()
        assert self.assert_in(
            expected=expected,
            actual=actual
        ), "登录后URL地址不对, expected=%r, actual=%r, 当前数据=%r" % (expected, actual, dt)

        expected = dt["expected_real_name"]
        assert self.assert_in(
            expected=expected,
            actual=actual_real_name
        ), "登录后 realname 地址不对, expected=%r, actual=%r, 当前数据=%r" % (expected, actual_real_name, dt)
