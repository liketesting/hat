from page.api.seniverse_weather_api import SeniverseWeatherApi

LANG_ZH_HANS = "zh-Hans"
UNIT_C = "c"

"""
心知天气 https://www.seniverse.com/
"""


def weather_now(request, location, language=LANG_ZH_HANS, unit=UNIT_C, app_key=None, logger=None):
    """
    心知天气 查询实时天气结果
    :param request:
    :param location:
    :param language:
    :param unit:
    :param app_key:
    :param logger:
    :return:
    """
    api = SeniverseWeatherApi(request, logger)
    api.set_key(app_key)
    return api.get_now(location=location, language=language, unit=unit)



