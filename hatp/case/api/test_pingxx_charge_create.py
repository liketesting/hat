import pytest

from base import read_csv, read_txt
from case import ApiCase
from page.api.pingxx import charge_get_after_create


class TestPingxxChargeCreate(ApiCase):
    """
    author: ArtLinty
    email: hello@linty.art
    desc: Ping++ 支付， Charge 类 测试
    """
    _csv_data = read_csv(
        __file__,
        "data/test_pingxx_charge_create.csv"
    )

    @pytest.fixture(autouse=True)
    def prepare(self):
        """
        测试固件
        :return:
        """
        self.init_logger(__name__)
        self.init_request(host="https://api.pingxx.com")
        yield

    @pytest.mark.parametrize("csv", _csv_data)
    def test_pingxx_charge_create(self, csv):
        """
        测试合法数据可以正确请求 charge.create 接口
        :return:
        """
        secret_key = csv["secret_key"]
        rsa_private_key = read_txt(current_file_path=__file__, txt_to_current=csv["private_key"])
        app_id = csv["app_id"]

        order_no = csv["order_no"] % self.current_time_string
        channel = csv["channel"]
        amount = csv["amount"]
        client_ip = csv["client_ip"]
        currency = csv["currency"]
        subject = csv["subject"] % order_no
        body = csv["body"] % order_no
        description = csv["description"] % order_no

        resp = charge_get_after_create(
            request=self.request,
            order_no=order_no,
            channel=channel,
            amount=amount,
            client_ip=client_ip,
            currency=currency,
            subject=subject,
            description=description,
            body=body,
            secret_key=secret_key,
            private_key=rsa_private_key,
            app_id=app_id,
            logger=self.logger
        )

        self.info(
            "[%r]-请求 charge.create, 使用数据 order_no=%r, channel=%r, amount=%r, client_ip=%r, currency=%r, subject=%r"
            % (__file__, order_no, channel, amount, client_ip, currency, subject)
        )

        expected = csv["expected_status_code"]
        actual = resp["status_code"]
        assert self.assert_equal(
            expected=expected,
            actual=actual
        ), "状态码断言失败: expected=%r, actual=%r, 当前数据是 %r! " % (expected, actual, csv)

        actual = resp["channel"]
        assert self.assert_equal(
            expected=channel,
            actual=actual
        ), "channel 断言失败: expected=%r, actual=%r, 当前数据是 %r! " % (channel, actual, csv)

        actual = resp["app"]
        assert self.assert_equal(
            expected=app_id,
            actual=actual
        ), "app_id 断言失败: expected=%r, actual=%r, 当前数据是 %r! " % (app_id, actual, csv)

        actual = resp["amount"]
        assert self.assert_int_equal(
            expected=amount,
            actual=actual
        ), "amount 断言失败: expected=%r, actual=%r, 当前数据是 %r! " % (amount, actual, csv)

        actual = resp["client_ip"]
        assert self.assert_equal(
            expected=client_ip,
            actual=actual
        ), "client_ip断言失败: expected=%r, actual=%r, 当前数据是 %r! " % (client_ip, actual, csv)

        actual = resp["currency"]
        assert self.assert_equal(
            expected=currency,
            actual=actual
        ), "currency断言失败: expected=%r, actual=%r, 当前数据是 %r! " % (currency, actual, csv)

        expected = self.json_to_dict(csv["expected_dict"])
        actual = resp["json_dict"]
        assert self.assert_json_equal(
            expected=expected,
            actual=actual,
            data_key="object"
        ), "object 断言失败: expected=%r, actual=%r, 当前数据是 %r! " % (expected, actual, csv)

        assert self.assert_json_equal(
            expected=expected,
            actual=actual,
            data_key="paid"
        ), "paid 断言失败: expected=%r, actual=%r, 当前数据是 %r! " % (expected, actual, csv)
