# hat project

#### Description
niushop 自动化测试项目，包含了 niushop 的 WebUI 自动化测试 和 WebApi 自动化测试方案

#### Software Architecture
- 使用 python 作为编程语言

- 使用 pytest 作为测试框架

- 使用 selenium 作为底层的 WebUI 库

- 使用 requests 作为底层的 WebApi 库

- 使用 yaml 作为业务组织的配置文件

- 使用 csv 存储测试用例使用的数据

- 使用 allure 作为报告输出的工具

- 具体的项目结构如下

  ```shell
  nspauto\: pycharm 的项目文件根目录
  |_ .idea\: pycharm 的项目配置目录 
  |_ base\: 底层支持库，可以直接用，一般不需要修改
  	|_ __init__.py: base 包的构造方法
  		|_ build_driver()
  		|_ build_request()
  		|_ Csv
  		|_ Json
  		|_ Path
  		|_ Yaml
  		|_ Db
  		|_ BoxDriver：建议调用的 selenium 底层库
  		|_ BoxRequest：建议调用的 requests 底层库
  		|_ Logger: 建议调用的 logging 底层库，做日志
  	|_ box.py: 底层的库封装
  		|_ _BoxDriver: 封装 selenium 的底层库，不建议直接调用
  		|_ _BoxRequest: 封装 requests 的底层库，不建议直接调用
  	|_ helper.py
  		|_ _CsvHelper
  		|_ _JsonHelper
  		|_ _PathHelper
  		|_ _YamlHelper
  		|_ _DbHelper		
  	|_ infra.py
  		|_ _Logger ：封装的 Logging 底层库
  		
  |_ case\: 测试用例脚本
  	|_ __init__.py: case 包的构造方法
  		|_ WebCase
  		|_ ApiCase
  		|_ _BaseCase: 是上面两个类的基类，不要直接用
  		
  |_ page\: 系统业务组织
  	|_ __init__.py: page 包的构造方法
  		|_ Page
  		|_ Api
  		|_ _BasePage: 是上面两个类的基类，不要直接用
  		
  |_ report\: 报告输出
  	|_ allure\: 存储 allure 的报告源文件
  	|_ log\：存储的 Logger 生成的 日志文件
  |_ usage\: 用法介绍
  |_ install.bat\: 部署使用项目之前，需要运行的 windows 脚本：安装所有的 python 依赖
  |_ main.bat\: 运行测试的脚本
  |_ requirements.txt\: 项目需要的所有 python 依赖
  
  ```

  

#### Installation

1. 安装 Python
2. 安装 PyCharm
3. 安装 chromedriver.exe 、geckodriver.exe 至少一个
4. 运行 install.bat



#### Instructions

1. 建议用 fork
2. git clone



#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)