import pytest

from base import read_csv
from case import ApiCase
from page.api import weather_now


class TestSeniverseWeatherGetNow(ApiCase):
    """
    author: ArtLinty
    email: hello@linty.art
    desc: 心知天气 天气类接口 now.json 测试
    """
    _csv_data = read_csv(
        __file__,
        "data/test_seniverse_weather_get_now.csv"
    )

    @pytest.fixture(autouse=True)
    def prepare(self):
        """
        测试固件
        :return:
        """
        self.init_logger(__name__)
        self.init_request(host="https://api.seniverse.com/")
        yield

    @pytest.mark.parametrize("csv", _csv_data)
    def test_seniverse_weather_get_now(self, csv):
        """
        测试合法数据可以正确请求 now.json 接口
        :return:
        """
        location = csv["location"]
        key = csv["key"]

        resp = weather_now(
            request=self.request,
            location=location,
            app_key=key,
            logger=self.logger
        )

        self.info(
            "[%r]-请求 now.json, 使用数据 location=%r, key=%r"
            % (__file__, location, key)
        )

        expected = csv["expected_status_code"]
        actual = resp["status_code"]
        assert self.assert_equal(
            expected=expected,
            actual=actual
        ), "状态码断言失败: expected=%r, actual=%r, 当前数据是 %r! " % (expected, actual, csv)

        expected = self.json_to_dict(csv["expected_dict"])
        actual = resp["json_dict"]
        assert self.assert_json_equal(
            expected=expected,
            actual=actual,
            data_key="results",
            index=0,
            sub_key="location/name",
        ), "location/name 断言失败: expected=%r, actual=%r, 当前数据是 %r! " % (expected, actual, csv)

        assert self.assert_json_equal(
            expected=expected,
            actual=actual,
            data_key="results",
            index=0,
            sub_key="location/country",
        ), "location/country 断言失败: expected=%r, actual=%r, 当前数据是 %r! " % (expected, actual, csv)
