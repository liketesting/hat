from base import read_yaml
from page.web.pingxx.index_page import IndexPage


class LoginPage(IndexPage):
    """
    Ping++ 登录页面
    """

    __config = read_yaml(__file__, "pingxx.yml", "LoginPage")

    def login(self, email, password):
        """
        登录
        :param email:
        :param password:
        :return:
        """
        element_dict = self.__config["ELEMENT"]
        self.driver.type(element_dict["EMAIL_INPUT"], email)
        self.driver.type(element_dict["PASSWORD_INPUT"], password)
        self.driver.click(element_dict["LOGIN_BUTTON"])

        self.info("[%r] - 通过登录页面进行登录 Ping++ 管理后台，email=%s, password=%s" % (__name__, email, password))
