from base import build_request, JsonHelper

"""
使用 base.build_request 直接请求 http
"""
token = "46c575222b62960bf8699eceae154d7c0239bf30ebda07be94901ec0"
url = "http://api.tushare.pro"

param = {
    "is_hs": "H",
    "list_status": "L",
    "exchange": "SSE"
}

data = {
    "api_name": "stock_basic",
    "token": token,
    "params": param,
    "fields": "ts_code,symbol,name,area,industry,fullname,enname,market,exchange,curr_type"
}

req = build_request(
    host=url
)

resp = req.send(
    uri="/",
    method="POST",
    params=data
)

print(resp["json_string"])
