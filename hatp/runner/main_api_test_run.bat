@echo off

python context.py

echo initialize report parameters
set reruns=3
set test_target=..\case\api\
set allure_target=..\report\allure\
set allure_report=c:\xampp\htdocs\allureport\api
if "%time:~,1%"==" " set time=0%time:~1,7%
set date_time=%date:~0,4%%date:~5,2%%date:~8,2%_%time:~0,2%%time:~3,2%%time:~6,2%
set xml_report=..\report\%date_time%\test_report_xml.xml
set log_report=..\report\%date_time%\test_report_log.log
set html_report=..\report\%date_time%\test_report_html.html

echo make directory for pytest result target
md %allure_target%..\%date_time%

echo remove all documents in allure result target root
del /a /f /q %allure_target%

echo remove all documents in allure report root
del /a /f /q %allure_report%

echo starting pytest
pytest %test_target% --reruns %reruns% --junitxml=%xml_report% --resultlog=%log_report% --html=%html_report% --alluredir=%allure_target%

echo finished pytest and starting generate allure report
allure generate --clean %allure_target% -o %allure_report%

echo report has been generated successfully!

