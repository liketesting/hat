from page.web.zentao_login_page import ZentaoLoginPage
from page.web.zentao_main_page import ZentaoMainPage


def login(driver, account, password, logger=None):
    """
    登录场景
    :param driver:
    :param account:
    :param password:
    :param logger:
    :return:
    """
    page = ZentaoLoginPage(
        driver=driver,
        logger=logger
    )

    return page.login(
        account=account,
        password=password
    )


def get_real_name(driver, logger=None):
    """
    获取真实姓名
    :param driver:
    :param logger:
    :return:
    """

    page = ZentaoMainPage(
        driver=driver,
        logger=logger
    )

    return page.get_real_name()


def is_menu_active(driver, menu, logger=None):
    """
    菜单是否激活场景
    :param driver:
    :param menu:
    :param logger:
    :return:
    """
    page = ZentaoMainPage(
        driver=driver,
        logger=logger
    )

    return page.is_menu_active(menu)


def get_real_name_with_login(driver, account, password, logger=None):
    """
    获取真实姓名在登录后
    :param driver:
    :param account:
    :param password:
    :param logger:
    :return:
    """

    page = ZentaoMainPage(
        driver=driver,
        logger=logger
    )
    page.login(account, password)

    return page.get_real_name()
