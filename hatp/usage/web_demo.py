import time

from base import build_driver
from scenario.zentao_biz import get_real_name_with_login

browser = build_driver(
    url="http://biz.demo.zentao.net/user-login.html",
    driver_type="c",
    wait_seconds=5
)

browser.type("account", "demo")
browser.save_window_snapshot("snapshot_%s.png" % time.time())

browser.type("x, //*[@id='loginPanel']/div/div[2]/form/table/tbody/tr[2]/td/input", "123456")
browser.save_window_snapshot("snapshot_%s.png" % time.time())

browser.click("s, #submit")
browser.save_window_snapshot("snapshot_%s.png" % time.time())

assert "my/" in browser.get_url(), "登录后 URL 跳转失败! "

browser.quit()

browser = build_driver("http://biz.demo.zentao.net/user-login.html", "hc")
actual_real_name = get_real_name_with_login(
    driver=browser,
    account="demo",
    password="123456"
)

print(actual_real_name)
