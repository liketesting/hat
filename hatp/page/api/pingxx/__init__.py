"""
Ping++ 支付 https://www.pingxx.com/
"""
from page.api.pingxx.charge_api import ChargeApi


def charge_create(request,
                  order_no,
                  channel,
                  amount,
                  client_ip,
                  currency,
                  subject,
                  body,
                  description=None,
                  secret_key=None,
                  private_key=None,
                  app_id=None,
                  logger=None):
    """
    Ping++ 创建支付
    :param request:
    :param order_no:
    :param channel:
    :param amount:
    :param client_ip:
    :param currency:
    :param subject:
    :param body:
    :param description:
    :param secret_key:
    :param private_key:
    :param app_id:
    :param logger:
    :return:
    """
    api = ChargeApi(request, logger)
    api.set_key(secret_key, private_key, app_id)
    return api.create(order_no,
                      channel,
                      amount,
                      client_ip,
                      currency,
                      subject,
                      body,
                      description)


def charge_get(request,
               charge_id,
               secret_key=None,
               private_key=None,
               app_id=None,
               logger=None):
    """
    Ping++ 查询单个支付信息
    :param request:
    :param charge_id: 支付 id
    :param secret_key:
    :param private_key:
    :param app_id:
    :param logger:
    :return:
    """
    api = ChargeApi(request, logger)
    api.set_key(secret_key, private_key, app_id)
    return api.query(charge_id)


def charge_get_after_create(request,
                            order_no,
                            channel,
                            amount,
                            client_ip,
                            currency,
                            subject,
                            body,
                            description=None,
                            secret_key=None,
                            private_key=None,
                            app_id=None,
                            logger=None):
    """
    Ping++ 创建支付后，进行查询当前支付，返回查询结果
    :param request:
    :param order_no:
    :param channel:
    :param amount:
    :param client_ip:
    :param currency:
    :param subject:
    :param body:
    :param description:
    :param secret_key:
    :param private_key:
    :param app_id:
    :param logger:
    :return:
    """
    api = ChargeApi(request, logger)
    api.set_key(secret_key, private_key, app_id)
    resp = api.create(order_no,
                      channel,
                      amount,
                      client_ip,
                      currency,
                      subject,
                      body,
                      description)
    charge_id = resp["id"]
    return api.query(charge_id)


def charge_query_list(
        request,
        secret_key,
        private_key,
        app_id,
        limit=10,
        reversed=None,
        channel=None,
        paid=None,
        refunded=None,
        created_gt=None,
        created_lt=None,
        logger=None):
    """

    :param request:
    :param secret_key:
    :param private_key:
    :param app_id:
    :param limit:
    :param reversed:
    :param channel:
    :param paid:
    :param refunded:
    :param created_gt:
    :param created_lt:
    :param logger:
    :return: dict
        :key 列表

    """

    # 实例化类：ChargeApi
    charge = ChargeApi(request=request, logger=logger)
    # 设置 私钥秘钥
    charge.set_key(secret_key=secret_key, private_key=private_key, app_id=app_id)
    # 调用 query_list()
    return charge.query_list(limit, reversed, channel, paid, refunded, created_gt, created_lt)

