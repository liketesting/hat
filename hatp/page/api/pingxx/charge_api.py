from urllib.parse import urlencode

from base import read_yaml, parse_json
from page import ApiPage


class ChargeApi(ApiPage):
    """
    author: ArtLinty
    email: hello@linty.art
    desc: Ping++ Charge API 业务
    """

    __config = read_yaml(
        current_file_path=__file__,
        yml_to_current="resource/config.yml",
        key_of_page="ChargeApi"
    )

    _secret_key = None
    _private_key = None
    _app_id = None

    def set_key(self, secret_key, private_key, app_id):
        """
        在调用其他方法之前，先调用此方法，初始化接口用到的 key
        :param secret_key: Ping++ 系统key
        :param private_key: 用户 RSA 私钥
        :param app_id: 用户的APP 的id
        :return:
        """
        self._secret_key = secret_key
        self._private_key = private_key
        self._app_id = {"id": app_id}

    def _get_headers_by_sign(self, uri, params=None):
        """
        根据签名和时间戳获取 headers
        :param params: 请求所需要的参数
        :param uri: 请求的 uri
        :return:
        """
        yml_header = self.__config["HEADER"]
        signature, timestamp = self._req.get_sign_by_pkcs115(
            rsa_private_key=self._private_key,
            uri=uri,
            body_params=params
        )
        headers_by_sign = {
            yml_header["SIGN"]: signature,
            yml_header["TIMESTAMP"]: timestamp
        }
        return headers_by_sign

    def create(self,
               order_no,
               channel,
               amount,
               client_ip,
               currency,
               subject,
               body,
               description=None):
        """
        请求 POST https://api.pingxx.com/v1/charges 的接口
        请首先调用 set_key()
        :param order_no:
        :param channel:
        :param amount:
        :param client_ip:
        :param currency:
        :param subject:
        :param body:
        :param description:
        :return:
        """
        yml = self.__config["API_CREATE"]
        yml_param = yml["PARAM"]
        yml_method = yml["METHOD"]

        params_from_yml = {
            yml_param["APP"]: self._app_id,
            yml_param["ORDER_NO"]: order_no,
            yml_param["CHANNEL"]: channel,
            yml_param["AMOUNT"]: amount,
            yml_param["CLIENT_IP"]: client_ip,
            yml_param["CURRENCY"]: currency,
            yml_param["SUBJECT"]: subject,
            yml_param["BODY"]: body,
            yml_param["DESCRIPTION"]: description
        }
        uri_from_yml = yml["URI"]
        params_from_yml = self._handle_params(params_from_yml)

        headers_by_sign = self._get_headers_by_sign(uri=uri_from_yml, params=params_from_yml)
        auth_from_param = (self._secret_key, "")

        self.info("[%s]-请求 charge.create 接口：url=%s, params=%r" % (__name__, uri_from_yml, params_from_yml))
        self.request.send(uri=uri_from_yml,
                          method=yml_method,
                          params=params_from_yml,
                          auth=auth_from_param,
                          headers=headers_by_sign)

        return self._parse_create(self._req.json_dict)

    def _parse_create(self, json_dict):
        """
        解析 POST https://api.pingxx.com/v1/charges 的接口返回值
        :param json_dict:
        :return:
        """

        resp = self._parse_http_resp()

        yml_resp = self.__config["API_CREATE"]["RESP"]
        data_key_list = yml_resp["DATA_KEY"]

        for key in data_key_list:
            value = parse_json(
                json_dict=json_dict,
                data_key=key
            )
            if value:
                resp[key] = value

        return resp

    def query(self, charge_id: str):
        """
        请求 GET https://api.pingxx.com/v1/charges/{CHARGE_ID} 接口
        请首先调用 init_key()
        :param charge_id:
        :return:
        """
        yml = self.__config["API_QUERY"]
        yml_method = yml["METHOD"]
        uri_from_yml = yml["URI"] % charge_id
        headers_by_sign = self._get_headers_by_sign(uri=uri_from_yml)
        auth_from_param = (self._secret_key, "")

        self.info("[%s]-请求 charge.create 接口：url=%s" % (__name__, uri_from_yml))
        self.request.send(uri=uri_from_yml,
                          method=yml_method,
                          auth=auth_from_param,
                          headers=headers_by_sign)

        return self._parse_query(self._req.json_dict)

    def _parse_query(self, json_dict):
        """
        解析 GET https://api.pingxx.com/v1/charges/{CHARGE_ID} 接口
        :param json_dict:
        :return:
        """
        resp = self._parse_http_resp()

        yml_resp = self.__config["API_QUERY"]["RESP"]
        data_key_list = yml_resp["DATA_KEY"]

        for key in data_key_list:
            value = parse_json(
                json_dict=json_dict,
                data_key=key
            )
            if value:
                resp[key] = value

        return resp

    def query_list(self, limit=None, reversed=None, channel=None, paid=None, refunded=None, created_gt=None,
                   created_lt=None):
        """

        :param limit:
        :param reversed:
        :param channel:
        :param paid:
        :param refunded:
        :param created_gt:
        :param created_lt:
        :return:
        """
        # 发请求

        # 准备 参数
        params = {
            "app[id]": self._app_id["id"],
            "limit": limit,
            "reversed": reversed,
            "channel": channel,
            "paid": paid,
            "refunded": refunded,
            "created[gt]": created_gt,
            "created[lt]": created_lt
        }
        params = self._handle_params(params)
        # 准备 URI
        uri = "/v1/charges?%s" % urlencode(params)
        # 准备 方法
        method = "GET"
        # 准备 首部
        headers = self._get_headers_by_sign(uri=uri)
        # 准备 认证
        auth = (self._secret_key, "")
        # 构建请求
        self.request.send(uri=uri, method=method, headers=headers, auth=auth)
        # 解析请求
        return self._parse_query_list(self.request.json_dict)

    def _parse_query_list(self, json_dict):
        """

        :param json_dict:
        :return:
        """
        resp = self._parse_http_resp()

        yml_resp = self.__config["API_QUERY_LIST"]["RESP"]
        data_key_list = yml_resp["DATA_KEY"]

        for key in data_key_list:
            value = parse_json(
                json_dict=json_dict,
                data_key=key
            )
            if value:
                resp[key] = value

        data_list = resp["data"]
        if data_list is not None:
            resp["data_count"] = len(data_list)
        else:
            resp["data_count"] = 0

        return resp
