from base import read_yaml
from page import WebPage


class ZentaoLoginPage(WebPage):
    """
    author: ArtLinty
    email: hello@linty.art
    desc: 禅道企业版 登录页面业务
    """
    __config = read_yaml(__file__, "web.yml", "ZentaoMainPage")

    def login(self, account, password):
        """
        登录
        :param account:
        :param password:
        :return: 当前 URL
        """

        browser = self.driver
        yml = self.__config

        yml_location = yml["LOCATOR"]
        browser.type(yml_location["ACCOUNT_INPUT"], account)
        browser.type(yml_location["PASSWORD_INPUT"], password)
        browser.click(yml_location["SUBMIT_BUTTON"])

        return self.current_url
