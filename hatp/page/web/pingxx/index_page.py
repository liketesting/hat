from urllib.parse import urljoin

from base import read_yaml, BoxDriver
from page import WebPage


class IndexPage(WebPage):
    """
    Pingx++ 主页
    """

    __config = read_yaml(__file__, "pingxx.yml", "IndexPage")

    def __init__(self, driver: BoxDriver, logger):
        """
        构造方法
        """
        super().__init__(driver=driver, logger=logger)
        if driver is not None:
            current_url = driver.get_url()
            driver.navigate(urljoin(current_url, self.__config["URI"]))

    def open_login(self):
        """
        打开登录页面
        :return:
        """
        element_dict = self.__config["ELEMENT"]
        self.driver.click(element_dict["LOGIN_MENU"])
        self.info("[%r] - 打开登录页面" % __name__)
