from base import read_yaml, parse_json
from page import ApiPage


class SeniverseWeatherApi(ApiPage):
    """
    author: ArtLinty
    email: hello@linty.art
    desc: 心知天气 天气类接口业务
    """
    LANG_ZH_HANS = "zh-Hans"
    UNIT_C = "c"

    __config = read_yaml(
        current_file_path=__file__,
        yml_to_current="api.yml",
        key_of_page="SeniverseWeatherApi"
    )

    _app_key = None

    def set_key(self, app_key):
        """
        初始化 key
        :param app_key:
        :return:
        """
        self._app_key = app_key

    def get_now(self, location, language=LANG_ZH_HANS, unit=UNIT_C):
        """
        获取指定城市的天气实况。
        要求：key 或者 （sign, uid, time_stamp ) 不可同时为 None
        使用前，先调用 init_key()
        请求示例：
        https://api.seniverse.com/v3/weather/now.json?key=Svt8nM15z02DyvYDt&location=beijing&language=zh-Hans&unit=c
        :param location:
        :param language:
        :param unit:
        :return:
            付费用户可获取全部数据，免费用户只返回天气现象文字、代码和气温3项数据。注：中国城市暂不支持云量和露点温度。
        """
        yml = self.__config["API_NOW"]
        yml_param = yml["PARAM"]
        yml_method = yml["METHOD"]
        params_from_yml = {
            yml_param["KEY"]: self._app_key,
            yml_param["LOCATION"]: location,
            yml_param["LANGUAGE"]: language,
            yml_param["UNIT"]: unit
        }
        params_from_yml = self._handle_params(params_from_yml)
        uri_from_yml = yml["URI"]
        self.info("[%s]-请求 now.json 接口：url=%s, params=%r" % (__name__, uri_from_yml, params_from_yml))

        self.request.send(uri=uri_from_yml, method=yml_method, params=params_from_yml)

        return self._parse_get_now(self._req.json_dict)

    def _parse_get_now(self, json_dict):
        """
        解析 get_now() 的接口返回值
        :param json_dict:
        :return:
        """
        resp = self._parse_http_resp()

        yml_resp = self.__config["API_NOW"]["RESP"]
        data_key = yml_resp["DATA_KEY"]
        index = yml_resp["INDEX"]
        sub_key_list = yml_resp["SUB_KEY"]

        for key in sub_key_list:
            value = parse_json(
                json_dict=json_dict,
                data_key=data_key,
                index=index,
                sub_key=key
            )
            if value:
                resp[key] = value

        return resp
