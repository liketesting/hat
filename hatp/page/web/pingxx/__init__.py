from page.web.pingxx.login_page import LoginPage


def web_login(browser, email, password, logger):
    """

    :param browser:
    :param email:
    :param password:
    :param logger:
    :return: current URL
    """
    web = LoginPage(driver=browser, logger=logger)
    web.open_login()
    web.login(email=email, password=password)
    return web.current_url
