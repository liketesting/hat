from base import build_driver, build_request


def test_open_chrome():
    """
    单元测试：检查 Chrome 浏览器
    :return:
    """
    chrome = build_driver("https://www.baidu.com", "c")
    expected = "百度一下"
    actual = chrome.get_title()
    chrome.quit()
    assert expected in actual, "chrome 浏览器打开网址错误，expected:%s, actual:%s" % (expected, actual)


def test_open_chrome_headless():
    """
    单元测试：检查 无头 Chrome 浏览器
    :return:
    """
    chrome = build_driver("https://www.baidu.com", "hc")
    expected = "百度一下"
    actual = chrome.get_title()
    chrome.quit()
    assert expected in actual, "chrome 浏览器打开网址错误，expected:%s, actual:%s" % (expected, actual)


def test_chrome():
    chrome = build_driver("http://www.baidu.com", 'c')
    chrome.quit()


def test_firefox():
    firefox = build_driver("http://www.baidu.com", 'f')
    firefox.quit()


def test_build_request_post():
    token_key = "46c575222b62960bf8699eceae154d7c0239bf30ebda07be94901ec0"
    api_url = "http://api.tushare.pro"
    stock_code = '601789.SH'
    api_name = "daily"
    args = {
        "ts_code": stock_code,
        "start_date": '20190101',
        "end_date": '20190308'
    }

    req_params = {
        'api_name': api_name,
        'token': token_key,
        'params': args,
        'fields': ""
    }

    # res 是请求返回来的响应数据，以下是 data_api.query() 里面的源代码
    req = build_request(api_url, method="POST", params=req_params)

    print(req.json_string)
    print(req.status_code)
    print(req.response_headers)
