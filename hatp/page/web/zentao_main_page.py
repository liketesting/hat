from base import read_yaml
from page.web.zentao_login_page import ZentaoLoginPage


class ZentaoMainPage(ZentaoLoginPage):
    """
    author: ArtLinty
    email: hello@linty.art
    desc: 禅道企业版 主页 业务
    """
    __config = read_yaml(__file__, "web.yml","ZentaoMainPage")


    def get_real_name(self):
        """
        获取右上角真实姓名
        :return:
        """
        browser = self.driver
        yml_locator = self.__config["LOCATOR"]

        return browser.get_text(yml_locator["REAL_NAME_SPAN"])

    def is_menu_active(self, menu):
        """
        判断菜单是否处于激活状态
        :param menu:
            my,
            product,
            project,
            qa,
            ops,
            oa,
            feedback,
            doc,
            report,
            company,
            admin
        :return:
        """
        browser = self.driver
        yml_locator = self.__config["LOCATOR"]
        if menu == "my":
            selector = yml_locator["MY_LI"]
        elif menu == "product":
            selector = yml_locator["PRODCUT_LI"]

        else:
            selector = None

        if selector:
            return "active" in browser.get_attribute(
                selector,
                "class"
            )

        return None
